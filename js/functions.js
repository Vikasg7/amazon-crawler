(function (require, module) {

   module.exports = {
      readline: {
         options: {
            objectMode: true
         },
         transform: function (data, enc, done) {
            var stream = this
            var lines = [stream.lastLine, data].join("").split(/\r?\n|\r(?!\n)/)
            stream.lastLine = lines.pop()
            lines.length && lines.forEach(function (line) { line && this.push(line) }, stream)
            done()
         },
         flush: function (done) {
            var stream = this
            if (stream.lastLine) { 
               stream.push(stream.lastLine)
               stream.lastLine = null
            }
            done()
         }
      }
   }

})(require, module)