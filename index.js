(function (require, module) {

   var api = require("amazon-product-api")
   var _ = require("./js/functions.js")
   var $ = require("streamify")
   var P = require("promise")
   var parse = JSON.parse
   var delayInSec = 1

   var credentials = parse(require("fs").readFileSync("./credentials.json", "UTF-8"))
   var client = api.createClient(credentials)

   process.stdin
      .setEncoding("UTF-8")
      .pipe($(_.readline))
      .pipe($(scrape))
      .on("finish", function () { console.error("Done!") })

   function scrape(upc, enc, getNextUPC) {
      P.resolve(upc)
       .then(makeRequest)
       .then(parseData.bind(null, upc))
       .catch(errHandler.bind(null, upc))
       .then(wait)
       .then(getNextUPC)
   }

   function makeRequest(upc) {
      var params = {
         idType: 'UPC',
         itemId: upc // "040071156213" // '043156171163'
      }
      return client.itemLookup(params)
   }

   function parseData(upc, results) {
      // require("fs").writeFileSync("output/output.json", JSON.stringify(results, null, 3), "UTF-8")
      if (!results.length) {
         console.error(upc, "-", "No results found")
      } else {
         var len = results.length
         for (var i = 0; i < len; i++) {
            var result = results[i]
            var row = {}
            row.ASIN = (result.ASIN || [])[0] || ""
            row.parentASIN = (result.ParentASIN || [])[0] || ""
            // row.pageURL = (result.DetailPageURL || [])[0]
            var attrs = (result.ItemAttributes || [])[0] || ""
            row.color = (attrs.Color || [])[0] | ""
            row.EAN = (attrs.EAN || [])[0] || ""
            row.feature = (attrs.Feature || []).join("\t").replace(/\n|"/g, "\t")
            row.price = attrs.ListPrice ? (attrs.ListPrice.FormattedPrice || [])[0] : ""
            row.NumberOfItems = (attrs.NumberOfItems || [])[0] || ""
            row.Title = ((attrs.Title || [])[0] || "").replace(/"/g, "'")
            row.PackageQuantity = (attrs.PackageQuantity || [])[0] || ""
            row.UPC = (attrs.UPC || [])[0] || ""
            console.log('"' + getValues(row).join('","') + '"')
         }
      }
   }

   function wait() {
      return new P(function (resolve, reject) {
         setTimeout(resolve, delayInSec * 1000)
      })
   }

   function errHandler(upc, error) {
      console.error(upc, "-", JSON.stringify(error))
      // console.error(error)
   }

   // Anciliary functions
   function getValues(object) {
      return Object.keys(object).map(function (key) { return object[key] })
   }

})(require, module)